import {Fragment} from "react";
import {Route, Routes, Navigate} from "react-router-dom";
import {useSelector} from "react-redux";



//layouts
import DropshipperLayout from "../layout/DropshipperLayout/DropshipperLayout";


//public pages
import LoginPage from "../pages/PublicPages/LoginPage";
import RegistrationPage from "../pages/PublicPages/RegistrationPage";


//admin pages
import ImportAdminPage from "../pages/AdminPages/ImportAdminPage";
import {ROLE} from "../utils/constants";


//dropshipper ImportAdminPage

import DropshipperImportPage from "../pages/DropshipperPages/DropshipperImportPage";
import DropshipperHomePage from "../pages/DropshipperPages/DropshipperHomePage";


const DropshipperRoutes = () => {
    return (
        <DropshipperLayout>
            <Routes>
                <Route path="/import" element={<DropshipperImportPage/>}/>
                <Route path="/" element={<DropshipperHomePage/>}/>
                <Route
                    path="*"
                    element={<Navigate to="/"/>}
                />
            </Routes>
        </DropshipperLayout>
    );
}

const AdminRoutes = () => {
    return (
        <Fragment>
            <Routes>
                <Route path="/import" element={<ImportAdminPage/>}/>
                <Route
                    path="*"
                    element={<Navigate to="/import"/>}
                />
            </Routes>
        </Fragment>
    );
}
const PublicRoutes = () => {
    return (
        <Fragment>
            <Routes>
                <Route path="/login" element={<LoginPage/>}/>
                <Route path="/registation" element={<RegistrationPage/>}/>
                <Route
                    path="*"
                    element={<Navigate to="/login"/>}
                />
            </Routes>
        </Fragment>
    );
}


const AppRoutes = () => {
    const data = useSelector((state: any) => {
        return state.userReducer;
    });

    let isAuthenticated = null;
    let roles = null;


    if(data.isAuthenticated && data.payload.user[0]){
        isAuthenticated = data.isAuthenticated;
        roles = data.payload.user[0];
    }

    if (isAuthenticated && roles.some((role:any) => role===ROLE.User)) {
        return <DropshipperRoutes/>;
    }
    if (isAuthenticated && roles.some((role:any) => role===ROLE.Admin)) {
        return <AdminRoutes/>;
    } else {
        return <PublicRoutes/>;
    }
};


export default AppRoutes;
