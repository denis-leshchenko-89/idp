import {BrowserRouter} from 'react-router-dom';
import store from "./store/store";
import "./assets/styles/scss/global.scss"
import {Provider} from 'react-redux'
import AppRoutes from './routes';
import requestInterceptor from './utils/config/api';
import "./assets/styles/scss/global.scss"
requestInterceptor();

function App() {

    return (
        <Provider store={store}>
            <BrowserRouter>
                <AppRoutes/>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
