import axios from 'axios';
import {API} from '../utils/constants'

class UserService {

    static logout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem("user");
    }

    static getToken = (user: any) => {
        const url = `${API.url}/api/login_check`;
        const response = axios.post(url, user);
        return response;
    };

    static getUser = (token: any) => {
        const url = `${API.url}/api/whoami`;
        const response = axios.get(url, {
            headers: {
                Authorization: 'Bearer ' + token //the token is a variable which holds the token
            }
        })
        return response;
    };

}

export default UserService;