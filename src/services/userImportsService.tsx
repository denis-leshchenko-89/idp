import axios from 'axios';
import {API} from '../utils/constants'

class userImportsService {

    static getImportsList = () => {
        const url = `${API.url}/api/user_imports`;
        const response = axios.get(url);
        return response;
    };

}

export default userImportsService;