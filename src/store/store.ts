import {applyMiddleware, createStore} from 'redux';
import rootReducer from './reducers'
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware  from 'redux-thunk';
import { createLogger } from 'redux-logger';

const loggerMiddleware = createLogger({
    duration: true,
    collapsed: true,
    colors: {
        title: () => '#139BFE',
        prevState: () => '#1C5FAF',
        action: () => '#149945',
        nextState: () => '#A47104',
        error: () => '#FF0005',
    },
});
;

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunkMiddleware,loggerMiddleware)));

export default store;