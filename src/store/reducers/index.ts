import {combineReducers} from 'redux'
import {userReducer} from "./userReducer";
import {userImportsReducer} from "./userImportsReducer";

const rootReducer = combineReducers({
    userImportsReducer,
    userReducer

})
export default rootReducer;