import {LOGIN_FAILURE, LOGOUT, LOGIN_SUCCESS, LOGIN_REQUEST} from '../constants';



const initialState = {
    isLoading : true,
    isAuthenticated: localStorage.getItem('token') ? true : false,
    payload: {
        token: localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token')|| '') : '',
        user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')|| '{}') : {}
    },
    error: undefined,
}

export function userReducer(state = initialState, action: any) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                payload: action.payload,
                user: action.payload.user,
                isLoading: false,
                isAuthenticated: true,
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false
            };
        default:
            return state
    }
}
