import {IMPORTS_REQUEST,IMPORTS_SUCCESS,IMPORTS_FAILURE} from '../constants';



const initialState = {
    isLoading : true,
    error: undefined,
    payload:{},
}

export function userImportsReducer(state = initialState, action: any) {
    switch (action.type) {
        case IMPORTS_REQUEST:
            return {
                ...state,
            };
        case IMPORTS_SUCCESS:
            return {
                ...state,
                payload: action.payload,
                isLoading: false,
            };
        case IMPORTS_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state
    }

}
