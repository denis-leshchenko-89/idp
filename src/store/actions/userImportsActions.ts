import {IMPORTS_REQUEST,IMPORTS_SUCCESS,IMPORTS_FAILURE} from '../constants';


import userImportsService from "../../services/userImportsService";


export const getUserImports = () => {
    return async (dispatch: any) => {
        dispatch(request());
        userImportsService.getImportsList().then((response)=>{
           dispatch(success(response.data));
        }).catch((error)=>{
            dispatch(failure(error));
        })
    }

    function request() {
        return {type: IMPORTS_REQUEST}
    }

    function success(payload: any) {
        return {type: IMPORTS_SUCCESS, payload}
    }

    function failure(error: any) {
        return {type: IMPORTS_FAILURE, error}
    }
}

