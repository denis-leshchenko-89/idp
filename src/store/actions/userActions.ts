import {LOGIN_FAILURE, LOGOUT, LOGIN_SUCCESS, LOGIN_REQUEST} from '../constants'
import {UserService} from "../../services";


export const login = (user: any) => {
    return async (dispatch: any) => {
        dispatch(request(user));
        UserService.getToken(user)
            .then(function (response) {
                const token = response.data.token;
                if (token) {
                    localStorage.setItem('token', JSON.stringify(token));
                }
                return response;
            })
            .then(function (response) {
                const token = response.data.token;
                UserService.getUser(token).then(function (response) {
                    if (response.data["hydra:member"]) {
                        const user = response.data["hydra:member"];
                        localStorage.setItem('user', JSON.stringify(user));
                        dispatch(success({user: user, token: token}));
                    }
                }).catch(function (error) {
                    dispatch(failure(error));
                });

            })
            .catch(function (error) {
                dispatch(failure(error));
            });
    }

    function request(user: any) {
        return {type: LOGIN_REQUEST, user}
    }

    function success(payload: any) {
        return {type: LOGIN_SUCCESS, payload}
    }

    function failure(error: any) {
        return {type: LOGIN_FAILURE, error}
    }
}


export const logout = () => {
    return (dispatch: any) => {
        UserService.logout();
        dispatch(logout());
    }
    function logout() {
        return {type: LOGOUT}
    }
}