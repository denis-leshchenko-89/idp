import './Loader.scss';
import ReactLoaderSpinner from "react-loader-spinner";

const Loader = ()=> {
    return (
        <div className="loader">
            <ReactLoaderSpinner
                type="ThreeDots"
                color="#000"
                height={100}
                width={100}
            />
        </div>
    )
};

export default Loader;