const ROLE = {
    Admin: 'ROLE_ADMIN',
    User: 'ROLE_USER'
}

export default ROLE
