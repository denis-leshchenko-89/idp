import axios from 'axios'

export default function requestInterceptor () {
    axios.interceptors.request.use((request:any) => {
        const token = localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token')|| '') : '';
        if (token) {
            request.headers.Authorization = 'Bearer ' + token;
        }
        return request
    })
}
