const translation = {
  /* ---------------------------------------- */
  /*              Header navigation           */
  /* ---------------------------------------- */
  navigation: {
    sale: 'Распродажа',
    novelty: 'Новинки',
    replenishmentOfGoods: 'Пополнения товаров',
    ttn: 'ТТН',
    orders: 'Заказы',
    salesChannels: 'Каналы сбыта',
  },

  /* ------------------------------------------------- */
  /*              User drop-down information           */
  /* ------------------------------------------------- */
  userInfo: {
    amountToBePaid: 'Сумма к выплате: ',
    balance: 'Баланс: ',
    replenish: 'Пополнить',
    orders: 'Заказы',
    basket: 'Корзина',
    ttn: 'ТТН',
    finance: 'Финансы',
    accounts: 'Счета',
    notifications: 'Уведомления',
    logout: 'Выйти из аккаунта',
  },
};

export { translation };
