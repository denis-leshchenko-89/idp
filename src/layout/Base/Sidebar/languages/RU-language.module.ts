const translation = {
  goods: {
    root: 'Товары',
    catalog: 'Каталог',
    inventories: 'Инвентари',
    favorites: 'Избранное',
    problemGoods: 'Проблемные товары',
    import: 'Импорт',
    export: 'Экспорт',
    pricingRules: 'Ценовые правила',
    salesChannels: 'Каналы сбыта',
  },
  orders: {
    root: 'Заказы',
    basket: 'Корзина',
    new: 'Новые',
    activeOrders: 'Активные заказы',
    completed: 'Выполненные',
    canceled: 'Отмененные',
    finance: 'Финансы',
    ttn: 'ТТН',
  },
  deferredPosting: {
    root: 'Отложенный постинг',
  },
  synchronization: {
    root: 'Синхронизации',
  },
  messages: {
    root: 'Сообщения',
  },
  employees: {
    root: 'Сотрудники',
  },
  settings: {
    root: 'Настройки',
    profile: 'Профиль',
    socialNetworks: 'Социальные сети',
    replenishBalance: 'Пополнить баланс',
    myAccounts: 'Мои счета',
    notifications: 'Уведомления',
  },
  blackList: {
    root: 'Черный список',
  },
  news: {
    root: 'Новости',
  },
  help: {
    root: 'Помощь',
    faq: 'Частые вопросы',
    technicalSupport: 'Техподдержка',
  },
};

export { translation };
