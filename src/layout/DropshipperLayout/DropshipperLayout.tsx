import './DropshipperLayout.scss';
import Sidebar from "../Base/Sidebar";
import Header from '../Base/Header';


const DropshipperLayout = ({children}: any) => {
    return (
        <div className='dropshipper-layout'>
            <Header/>
            <main className="main">
                <Sidebar/>
                <div className="content">
                    {children}
                </div>
            </main>
        </div>
    )
};

export default DropshipperLayout;

