import './ImportAdminPage.scss'
import {useDispatch} from "react-redux";
import {logout} from "../../../store/actions/userActions";


const ImportAdminPage = () => {
    const dispatch = useDispatch();

    const handleLogOut = (event: any) => {
        event.preventDefault();
        dispatch(logout());
    };

    return (
        <div className='import-page'>
            <h1>Admin Dashboard</h1>
            <a onClick={handleLogOut}>Выход</a>
        </div>
    )
};

export default ImportAdminPage;