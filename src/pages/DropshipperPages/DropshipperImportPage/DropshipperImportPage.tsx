import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../../store/actions/userActions";
import './DropshipperImportPage.scss';
import {useEffect} from "react";
import {getUserImports} from "../../../store/actions/userImportsActions";
import Loader from "../../../componets/BaseComponents/Loader";
import {Link} from "react-router-dom";


const DropshipperImportPage = () => {
    const dispatch = useDispatch();

    const isLoading: any = useSelector((state: any) => {
        return state.userImportsReducer.isLoading;
    });

    const lists: any = useSelector((state: any) => {
        return state.userImportsReducer.payload['hydra:member'];
    });


    useEffect(() => {
        dispatch(getUserImports());
    }, [dispatch])

    const handleLogOut = (event: any) => {
        event.preventDefault();
        dispatch(logout());
    };

    return (
        <div className='import-page'>
            {isLoading && <Loader/>}
            <h1>Dropshipper Import Page</h1>
            <h2>Меню:</h2>
            <nav>
                <Link to="/">Домашная страница</Link>
                <a onClick={handleLogOut}>Выход</a>
            </nav>

            {lists && lists.map((listItem: any) => {
                return <div key={listItem.id}>ID:{listItem.id}<br/>URL:{listItem['@id']}"</div>
            })}
        </div>

    )
};

export default DropshipperImportPage;
