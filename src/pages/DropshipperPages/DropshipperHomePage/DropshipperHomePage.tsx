import {useDispatch} from "react-redux";
import {logout} from "../../../store/actions/userActions";
import './DropshipperHomePage.scss';
import {Link} from "react-router-dom";


const DropshipperHomePage = () => {
    const dispatch = useDispatch();


    const handleLogOut = (event: any) => {
        event.preventDefault();
        dispatch(logout());
    };

    return (
        <div className='dropsipper-home'>
            <h1>Dropshipper Home Page</h1>
            <h2>Меню:</h2>
            <nav>
                <Link to="/import">Страница Импорта</Link>
                <a onClick={handleLogOut}>Выход</a>
            </nav>
        </div>

    )
};

export default DropshipperHomePage;
