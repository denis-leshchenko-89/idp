import React, {useState} from 'react';

import './LoginPage.scss'
import {login} from "../../../store/actions/userActions";
import { useDispatch } from 'react-redux';

const LoginPage = () => {

    const initialState = {
        email: "",
        password: "",
    };
    const [{email, password}, setState] = useState(initialState);
    const dispatch = useDispatch();

    const handleSubmit = (event: any) => {
        event.preventDefault();
        dispatch(login({email, password}));
    }

    const onChange = (event: any) => {
        const {name, value} = event.target;
        setState(prevState => ({...prevState, [name]: value}));
    };

    return (
        <div className='login-page'>
            <div className="form">
                <form className="login" onSubmit={handleSubmit}>
                    <input type="text" name="email" value={email} placeholder="Email" onChange={onChange}/>
                    <input type="password" name="password" value={password} placeholder="Password" onChange={onChange}/>
                    <button type="submit" className="submit">Вход</button>
                </form>
            </div>
        </div>
    )
};

export default LoginPage;